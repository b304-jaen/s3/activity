package com.zuitt.WDC043_S3_A1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class FactorialNumber {

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");

        try{

            int num = in.nextInt();
            int answer = 1;

            if(num < 0){
                throw new IllegalArgumentException("Input must be a positive integer");
            }
                for (int i = num; i > 0; i--) {
                    answer *= i;
                }
                System.out.println("The factorial of " + num + " is " + answer);


        }
        catch(InputMismatchException e){
            System.out.println("Enter a valid integer greater than 0");
            e.printStackTrace();
        }catch(IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

    }

}
